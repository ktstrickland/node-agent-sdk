const Agent = require('./../../lib/AgentSDK');

const agent = new Agent({
    accountId: process.env.LP_ACCOUNT,
    username: process.env.LP_USER,
    password: process.env.LP_PASSWORD,
    csdsDomain: process.env.LP_CSDS
});

const maxAge = 10; // seconds
const freq = 2; // seconds
let openConvs = {};
let myConvs;
// let myConvsSub;

(function () {

    agent.on('connected', () => {
        console.log('Agent connected...');
        agent.setAgentState({ availability: 'AWAY' }); // Do not route me conversations, I'll join by myself.
        agent.on('cqm.ExConversationChangeNotification', notificationBody => {
            notificationBody.changes.forEach(change => {
                if (change.type !== 'DELETE') {
                    if (!openConvs[change.result.convId]) {
                        openConvs[change.result.convId] = change.result;
                        closeConvs(change.result);
                    }
                }
                else if (change.type === 'DELETE') {
                    delete openConvs[change.result.convId];
                }
            });
            if (!myConvs) {
                myConvs = setInterval(checkConvs, freq*1000);
            }
        });
        agent.subscribeExConversations({
            'convState': ['OPEN'] // subscribes to all open conversation in the account.
        });
        agent._pingClock = setInterval(agent.getClock, 30000);
    });

    agent.on('error', err => {
        console.log('Error', err);
    });

    agent.on('closed', data => {
        // For production environments ensure that you implement reconnect logic according to
        // liveperson's retry policy guidelines: https://developers.liveperson.com/guides-retry-policy.html
        console.log('Socket closed', data);
        clearInterval(agent._pingClock);
        agent.reconnect(); //regenerate token for reasons of authorization (data === 4401 || data === 4407)
    });
}());

function closeConvs(result) {
    // console.log('Entering closeConvs()');
    var ts = Math.round((new Date()).getTime() / 1000);
    var lu = Math.round(result.conversationDetails.startTs/1000);
    var age = ts-lu;
    console.log('Conv '+result.convId+' last updated '+ age +' seconds ago.');
    if (age >= maxAge){
        agent.updateConversationField({
            'conversationId': result.convId,
            'conversationField': [{
                'field': 'ParticipantsChange',
                'type': 'ADD',
                'role': 'MANAGER'
            }]
        }, () => {
            agent.updateConversationField({
                'conversationId': result.convId,
                'conversationField': [{
                    'field': 'ConversationStateField',
                    'conversationState': 'CLOSE'
                }]
            });
        });
        console.log('Bot closed conversation '+result.convId+'.');
    } else {
        agent.updateConversationField({
            'conversationId': result.convId,
            'conversationField': [{
                'field': 'ParticipantsChange',
                'type': 'ADD',
                'role': 'MANAGER'
            }]
        }, () => { 
            agent.updateConversationField({
                'conversationId': result.convId,
                'conversationField': [{
                    'field': 'ParticipantsChange',
                    'type': 'REMOVE',
                    'role': 'MANAGER'
                }]
            });
        });
    }
}

function checkConvs() {
    // console.log(openConvs=='{}');
    if (Object.keys(openConvs).count>0){
        Object.keys(openConvs).forEach(function(key) {
            var result = openConvs[key];
            closeConvs(result);
        });
    } else {
        console.log('Resubscribing...');
        agent.subscribeExConversations({
            'convState': ['OPEN']
        });
    }
}